import java.io.File;
import java.util.*;

import static java.lang.Integer.parseInt;

public class Main {
    public static void displayResult(HashMap<String , Integer>hashMap){
        for(Map.Entry<String,Integer> set:hashMap.entrySet()){
            System.out.println(set.getKey()+"="+set.getValue());
        }
    }
    public static ArrayList<Object> matchDataFunction(){
        HashMap<String, Integer> resultObj = new HashMap<>();
        ArrayList<Object> finalArray=new ArrayList<>();
        ArrayList<Object> arrayList=new ArrayList<Object>();
        try {
            Scanner scanner = new Scanner(new File("matches.csv"));
            while(scanner.hasNextLine())
            {
                arrayList.add(scanner.nextLine());

            }

        } catch (Exception e){
            System.out.println(e);
        }
        return arrayList;
    }
    public static ArrayList<Object> deliveryDataFunction(){
        HashMap<String, Integer> resultObj = new HashMap<>();
        ArrayList<Object> finalArray=new ArrayList<>();
        ArrayList<Object> arrayList=new ArrayList<Object>();
        try {
            Scanner scanner = new Scanner(new File("deliveries.csv"));
            while(scanner.hasNextLine())
            {
                arrayList.add(scanner.nextLine());

            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return arrayList;
    }
    public static HashMap<String, Integer> matchesPerYear(){
        ArrayList<Object> matchArrayList = matchDataFunction();
        ArrayList<Object> keyValues = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();
        HashMap<String ,Integer> resultObj = new HashMap<>();
        keyValues.add(matchArrayList.get(0));

        String keyStr = keyValues.toString();
        String[] keyArray = keyStr.split(",");
        for (int listIndex=1; listIndex<matchArrayList.size();listIndex++){
            String tempValue = matchArrayList.get(listIndex).toString();
            String[] tempArr = tempValue.split(",");
            for (int tempArrIndex=0; tempArrIndex<tempArr.length; tempArrIndex++){
                hashMap.put(keyArray[tempArrIndex], tempArr[tempArrIndex]);
            }

            if(resultObj.containsKey(hashMap.get("season"))){
                resultObj.put(hashMap.get("season"),resultObj.get(hashMap.get("season"))+1);
            } else {
                resultObj.putIfAbsent(hashMap.get("season"),1);
            }

        }
        displayResult(resultObj);
        return resultObj;


    }
    public static HashMap<String ,Integer> matchesWonPerTeam(){
        ArrayList<Object> matchArrayList = matchDataFunction();
        ArrayList<Object> keyValues = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();
        HashMap<String ,Integer> resultObj = new HashMap<>();
        keyValues.add(matchArrayList.get(0));

        String keyStr = keyValues.toString();
        String[] keyArray = keyStr.split(",");
        for (int listIndex=1; listIndex<matchArrayList.size();listIndex++){
            String tempValue = matchArrayList.get(listIndex).toString();
            String[] tempArr = tempValue.split(",");
            for (int tempArrIndex=0; tempArrIndex<tempArr.length; tempArrIndex++){
                hashMap.put(keyArray[tempArrIndex], tempArr[tempArrIndex]);
            }

            if(resultObj.containsKey(hashMap.get("winner"))){
                resultObj.put(hashMap.get("winner"),resultObj.get(hashMap.get("winner"))+1);
            } else {
                resultObj.putIfAbsent(hashMap.get("winner"),1);
            }
        }
        displayResult(resultObj);

        return resultObj;

    }
    public static HashMap<String ,Integer>  venueUsedIn2016(){
        ArrayList<Object> matchArrayList = matchDataFunction();
        ArrayList<Object> keyValues = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();
        HashMap<String ,Integer> resultObj = new HashMap<>();
        keyValues.add(matchArrayList.get(0));

        String keyStr = keyValues.toString();
        String[] keyArray = keyStr.split(",");
        for (int listIndex=1; listIndex<matchArrayList.size();listIndex++) {
            String tempValue = matchArrayList.get(listIndex).toString();
            String[] tempArr = tempValue.split(",");
            for (int tempArrIndex = 0; tempArrIndex < tempArr.length; tempArrIndex++) {
                hashMap.put(keyArray[tempArrIndex], tempArr[tempArrIndex]);
            }

            if (hashMap.get("season").equals("2016")) {

                if(resultObj.containsKey(hashMap.get("venue"))){
                    resultObj.put(hashMap.get("venue"),resultObj.get(hashMap.get("venue"))+1);
                } else {
                    resultObj.putIfAbsent(hashMap.get("venue"),1);
                }
            }

        }
        displayResult(resultObj);

        return resultObj;

    }
    public static HashMap<String, Integer> extraRunsIn2016(){
        ArrayList<Object> matchArrayList = matchDataFunction();
        ArrayList<Object> deliveryArrayList = deliveryDataFunction();
        ArrayList<Object> keyValues = new ArrayList<>();
        HashMap<String, String> hashMap1 = new HashMap<>();
        HashMap<String ,HashMap> resultObj = new HashMap<>();
        HashMap<String,HashMap> finalObj = new HashMap<>();
        HashMap<String,Integer> extraRunsObject = new HashMap<>();
        keyValues.add(deliveryArrayList.get(0));


        String keyStr = keyValues.toString();

        String[] keyArray = keyStr.split(",");

        for (int listIndex=1; listIndex<deliveryArrayList.size();listIndex++){
            String tempValue = deliveryArrayList.get(listIndex).toString();
            String[] tempArr = tempValue.split(",");

            for (int tempArrIndex=0; tempArrIndex<tempArr.length; tempArrIndex++){
                hashMap1.put(keyArray[tempArrIndex], tempArr[tempArrIndex]);
            }

            if(resultObj.containsKey(hashMap1.get("[    match_id"))){


                if(resultObj.get(hashMap1.get("[    match_id")).containsKey(hashMap1.get("bowling_team"))){


                    resultObj.get(hashMap1.get("[    match_id")).putIfAbsent(hashMap1.get("bowling_team"), parseInt(hashMap1.get("extra_runs"))+parseInt((resultObj.get(hashMap1.get("[    match_id")).get(hashMap1.get("bowling_team"))).toString()));
                } else {
                    resultObj.get(hashMap1.get("[    match_id")).putIfAbsent(hashMap1.get("bowling_team"), parseInt(hashMap1.get("extra_runs")));

                }
            } else {
                resultObj.putIfAbsent(hashMap1.get("[    match_id"),new HashMap<String,Integer>());


            }

        }

        for (int matchListIndex=1; matchListIndex<matchArrayList.size();matchListIndex++){
            String tempValue = matchArrayList.get(matchListIndex).toString();


            String[] tempArr = tempValue.split(",");
            if(parseInt(tempArr[1])==2016){
                if(resultObj.containsKey(tempArr[0])){
                    finalObj.put(tempArr[0],resultObj.get(tempArr[0]));
                }
            }
        }

        for (HashMap.Entry<String, HashMap> setOne : finalObj.entrySet()) {


            for (Object setTwo : setOne.getValue().entrySet()){
                HashMap<String, Integer> tempHashMap = new HashMap<>();
                setTwo.toString().split("=");
                tempHashMap.put( setTwo.toString().split("=")[0], parseInt( setTwo.toString().split("=")[1]));
                if(extraRunsObject.containsKey(setTwo.toString().split("=")[0])){
                    extraRunsObject.put(setTwo.toString().split("=")[0],extraRunsObject.get(setTwo.toString().split("=")[0])+parseInt( setTwo.toString().split("=")[1]));
                } else {
                    extraRunsObject.putIfAbsent(setTwo.toString().split("=")[0],parseInt( setTwo.toString().split("=")[1]));
                }

            }

        }


        displayResult(extraRunsObject);

        return extraRunsObject;
    }








    public static HashMap<String, Integer> topEconomy() {
        ArrayList<Object> matchArrayList = matchDataFunction();
        ArrayList<Object> deliveryArrayList = deliveryDataFunction();
        ArrayList<Object> keyValues = new ArrayList<>();
        ArrayList<String> finalArray = new ArrayList<>();
        HashMap<String, Integer> extraRunsObject = new HashMap<>();
        HashMap<String , HashMap> bowlersObject = new HashMap<>();
        HashMap<String, Integer> economyObject = new HashMap<>();
        keyValues.add(deliveryArrayList.get(0));


        for (int matchListIndex=1; matchListIndex<matchArrayList.size();matchListIndex++){
            String tempValue = matchArrayList.get(matchListIndex).toString();


            String[] tempArr = tempValue.split(",");
            if(parseInt(tempArr[1])==2016){

                finalArray.add(tempArr[0]);

            }

        }

        for(int deliveryArrayIndex= 0;deliveryArrayIndex<deliveryArrayList.size();deliveryArrayIndex++ ) {
            int wide_runs=parseInt(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[11]);
            int leg_bye=parseInt(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[13]);
            int bye_runs=parseInt(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[12]);
            int no_ball=parseInt(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[14]);
            int batsman_runs=parseInt(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[16]);
            int extra_runs=parseInt(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[17]);
//            System.out.println(deliveryArrayList.get(deliveryArrayIndex));
            if (finalArray.contains(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[0])) {

                if (bowlersObject.containsKey(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8])) {
                    if(wide_runs==0&&no_ball==0){
                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("balls", Integer.parseInt(bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).get("balls").toString())+1);
                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("runs",Integer.parseInt(bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).get("runs").toString())+batsman_runs+extra_runs);
                    }
                    if(wide_runs==1&&no_ball==0){

                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("runs",Integer.parseInt(bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).get("runs").toString())+batsman_runs+extra_runs);
                    }
                    if(wide_runs==0&&no_ball==1){

                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("runs",Integer.parseInt(bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).get("runs").toString())+batsman_runs+extra_runs);
                    }

                } else {
                    bowlersObject.putIfAbsent(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8],new HashMap<>());

                    if(wide_runs==0&&leg_bye==0&&bye_runs==0&&no_ball==0) {
                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("balls", 1);
                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("runs", batsman_runs+extra_runs);
                    }
                    if(wide_runs==0&&extra_runs!=0&&no_ball==0) {
                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("balls", 1);
                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("runs", 0);
                    }
//





                }

            }
        }


        System.out.println(bowlersObject);
        for(HashMap.Entry<String, HashMap> setOne : bowlersObject.entrySet())
        {
            economyObject.putIfAbsent(setOne.getKey(), (Integer) setOne.getValue().get("balls")/(Integer) setOne.getValue().get("runs"));


        }
        return economyObject;
    }



    public static void main(String[] args){
//     matchesPerYear(); 
//     matchesWonPerTeam();
//     venueUsedIn2016();
//     extraRunsIn2016();
//     topEconomy();

    }
}
